CREATE TABLE Player (
  username TEXT PRIMARY KEY,
  balance INT CHECK (balance >= 0)
);

CREATE TABLE Shop (
  product TEXT PRIMARY KEY,
  in_stock INT CHECK(in_stock >= 0),
  price INT CHECK (price >= 0)
);

CREATE TABLE Inventory (
  username TEXT REFERENCES Player,
  product TEXT REFERENCES Shop,
  amount INT CHECK (amount >= 0),
  PRIMARY KEY (username, product)
);

INSERT INTO Player (username, balance)
VALUES ('Alice', 100);

INSERT INTO Player (username, balance)
VALUES ('Bob', 200);

INSERT INTO Shop (product, in_stock, price)
VALUES ('marshmello', 10, 10);
