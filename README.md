# Homework
### Run postgresql
```
docker-compose up
```
### Install dependencies:
```
pip install -r requirements.txt
```
### Run program
```
python main.py
```